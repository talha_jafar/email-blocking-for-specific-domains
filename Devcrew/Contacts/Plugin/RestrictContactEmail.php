<?php

namespace Devcrew\Contacts\Plugin;

use Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Framework\UrlFactory;
use Magento\Framework\Message\ManagerInterface;

class RestrictContactEmail
{

    /** @var \Magento\Framework\UrlInterface */
    protected $urlModel;

    /**
     * @var \Magento\Framework\Controller\Result\RedirectFactory
     */
    protected $resultRedirectFactory;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;


     private $logger;
    /**
     * RestrictCustomerEmail constructor.
     * @param UrlFactory $urlFactory
     * @param RedirectFactory $redirectFactory
     * @param ManagerInterface $messageManager
     */
    public function __construct(
        UrlFactory $urlFactory,
        RedirectFactory $redirectFactory,
        ManagerInterface $messageManager,
        \Psr\Log\LoggerInterface $logger

    )
    {
        $this->urlModel = $urlFactory->create();
        $this->resultRedirectFactory = $redirectFactory;
        $this->messageManager = $messageManager;
        $this->logger = $logger;
    }

    /**
     * @param \Magento\Customer\Controller\Account\CreatePost $subject
     * @param \Closure $proceed
     * @return mixed
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function aroundExecute(
        \Magento\Contact\Controller\Index\Post $subject,
        \Closure $proceed
    )
    {
        /** @var \Magento\Framework\App\RequestInterface $request */
        $email = $subject->getRequest()->getParam('email');
        $pieces = explode("@", $email);


        if($pieces[1] == 'list.ru' || $pieces[1] == 'bk.ru' || $pieces[1] == 'yandex.ru' || $pieces[1] == 'mail.ru' ){
        	$this->messageManager->addErrorMessage(
                'Registration is disabled for you domain'
            );
            $defaultUrl = $this->urlModel->getUrl('*/*/create', ['_secure' => true]);
            /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
            $resultRedirect = $this->resultRedirectFactory->create();

            return $this->resultRedirectFactory->create()->setPath('contact/index');
        }
        
    
        // list($nick, $domain) = explode('@', $email, 2);
        // if (in_array($domain, ['list.ru', 'bk.ru', 'yandex.ru', 'mail.ru'], true)) {

        // 		die("found");
        // 	}
        // 	else{
        // 		die("not found");
        // 	}
        // 	   //  $this->messageManager->addErrorMessage(
        // 	}
            //     'Contact is disabled for you domain'
            // );
            // $defaultUrl = $this->urlModel->getUrl('*/*/create', ['_secure' => true]);
            // * @var \Magento\Framework\Controller\Result\Redirect $resultRedirect 
            // $resultRedirect = $this->resultRedirectFactory->create();

            // return $resultRedirect->setUrl($defaultUrl);

        
        return $proceed();
    }
}